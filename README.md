# R&R SPARC workshop material by D. Nüst

REPLICABILITY AND REPRODUCIBILITY IN GEOSPATIAL RESEARCH: A SPARC WORKSHOP

Dates: Feb 11 - Feb 12, 2019 
Location: Arizona State University (ASU) Tempe Campus
Host: ASU Spatial Research Analysis Center (SPARC), https://sgsup.asu.edu/SPARC

## Files

[View **slides** online](https://nuest.gitlab.io/rr-geospatial-sparc/) or [download as PDF](https://nuest.gitlab.io/rr-geospatial-sparc/2019-02_nuest_rr-sparc.pdf).

Read Daniel's [**position paper**](https://nuest.gitlab.io/rr-geospatial-sparc//position-paper.html) or [download as PDF](https://nuest.gitlab.io/rr-geospatial-sparc//position-paper.pdf).

[See `.gitlab-ci.yml` for commands to create HTML and PDF output files.]

## License

![](https://licensebuttons.net/l/by/4.0/88x31.png)

This work is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).
